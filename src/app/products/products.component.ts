import { Component, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {
  // url = 'http://search-bellashus-aorqc2qtpvydg3ipi5y6i5itbu.eu-west-1.es.amazonaws.com/_all/_search';
  url = 'http://search-gresvigelasticsearch23-4bfdjqvzjneu3l45o6hgmwa3ha.eu-west-1.es.amazonaws.com:80';
  params = '/prodintersport_idx1,prodintersport_idx2/product/_search';
  results: string[];
  resultsTotal: number;
  timer;
  isLoaded = true;

  constructor(private http: HttpClient) { }

  ngOnInit() {}

  queryBuild($param) {
    return {
      query: {
        multi_match : {
          query : $param,
          fields : [ 'name', 'sku' ]
        }
      },
      size : 5,
    };
  }

  setResultsLocal($product) {
    localStorage.setItem('product_detail', JSON.stringify($product));
  }

  searchProduct($query) {
    this.isLoaded = false;
    this.http.post(this.url + this.params, this.queryBuild($query) ).subscribe(data => {
      this.results = data['hits']['hits'];
      this.resultsTotal = data['hits']['total'];
      this.isLoaded = true;
    }, err => {
      console.log('getting data error');
    });
  }

  getQuery(event) {
    window.clearTimeout(this.timer);
    const $this = this;
    this.timer = window.setTimeout(function () {
      if ($this.isQueryValid(event.target.value)) {
        $this.searchProduct(event.target.value);
      }else {
        $this.resultsTotal = 0;
      }
    }, 250);
  }

  isQueryValid($query) {
    return $query.length >= 3;
  }

}
