import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './/app-routing.module';
import { ProductsComponent } from './products/products.component';
import { HeaderComponent } from './layout/header.component';
import { ProductsDetailComponent } from './products-detail/products-detail.component';
import { DataService } from './services/data.service';
import { DataApiService } from './services/data-api.service';
import { GoogleApiService } from './services/google-api.service';
import { GoogleService } from './services/google.service';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProductsComponent,
    HeaderComponent,
    ProductsDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [DataService, DataApiService, GoogleApiService, GoogleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
