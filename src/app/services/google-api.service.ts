import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map} from 'rxjs/operators';

@Injectable()
export class GoogleApiService {
    url = '/dashboard/ga/?data=ga';

    constructor(private http: HttpClient) { }

    getData(): Observable<Data> {
        return this.http.get<Data>(this.url).pipe(
          map(res => new Data(res))
        );
    }
}

export class Data {
    visitors_gsport: number;
    visitors_intersport: number;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
