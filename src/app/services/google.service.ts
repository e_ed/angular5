import { Injectable } from '@angular/core';
import { GoogleApiService } from './google-api.service';

@Injectable()
export class GoogleService {
    constructor(private service: GoogleApiService) {}
    getData() {
      return this.service.getData();
    }
}
