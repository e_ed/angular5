import { Injectable } from '@angular/core';
import { DataApiService } from './data-api.service';

@Injectable()
export class DataService {
    constructor(private service: DataApiService) {}
    getData() {
      return this.service.getData();
    }
}
