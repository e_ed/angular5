import { Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map} from 'rxjs/operators';

@Injectable()
export class DataApiService {
    url = '/dashboard/ga/?data=magento';
    constructor(private http: HttpClient) { }

    getData(): Observable<Data> {
        return this.http.get<Data>(this.url)
            .pipe(
              map(res => new Data(res))
            );
    }
}

export class Data {
    orders: {
      last_hour: number;
      last_minute: number;
      last_opened: string[];
    };
    pos: {
      last_import: string;
    };
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
