import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsDetailComponent implements OnInit {
  url = 'http://search-gresvigelasticsearch23-4bfdjqvzjneu3l45o6hgmwa3ha.eu-west-1.es.amazonaws.com:80/prodintersport_idx1,prodintersport_idx2/product/_search?q=id';
  product: any;
  param: number;
  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.param = parseInt(res.id, 10);
    });
    // console.log(this.param);
    this.getResultsLocal();
    this.isParamValid(this.param);
  }

  isParamValid(param) {
    if (this.param && this.product.id !== this.param) {
      if (Number.isInteger(this.param)) {
        this.getProduct(this.param);
      }
    }
  }

  getProduct($id) {
    this.http.get(this.url + $id).subscribe(res => {
      this.product = res['hits']['hits'][0]['_source'];
    });
  }

  getResultsLocal() {
    this.product = JSON.parse(localStorage.getItem('product_detail'));
  }

}
