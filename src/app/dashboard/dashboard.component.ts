import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { GoogleService } from '../services/google.service';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  title = 'Dashboard';
  refreshTimeOrders = 30000;
  refreshTimeGA = 30000;
  orders: any;
  pos_last_update: string;
  visitors_gsport: number;
  visitors_intersport: number;
  isLoadedOrders = false;
  isLoadedGA = false;
  constructor(
    private service: DataService,
    private googleService: GoogleService
  ) {}

  ngOnInit() {
    this.initOrders(this.refreshTimeOrders);
    this.initVisitors(this.refreshTimeGA);
  }

  initOrders(refresh_time) {
    const $this = this;
    this.getOrders();
    setInterval(function () {
      $this.getOrders();
    }, refresh_time);
  }

  getOrders() {
    this.isLoadedOrders = false;
    this.service.getData().subscribe(
      data => {
        this.isLoadedOrders = true;
        this.orders = data.orders;
        this.pos_last_update = this.countDateDiff(data.pos.last_import);
      }
    );
  }

  initVisitors(refresh_time) {
    const $this = this;
    this.getVisitors();
    setInterval(function () {
      $this.getVisitors();
    }, refresh_time);
  }

  getVisitors() {
    this.isLoadedGA = false;
    this.googleService.getData().subscribe(
      data => {
        this.isLoadedGA = true;
        this.visitors_gsport = data.visitors_gsport;
        this.visitors_intersport = data.visitors_intersport;
      }
    );
  }

  countDateDiff($date) {
    moment.locale('nb');
    const oldFormatted = moment($date).format('YYYY-M-D HH:mm:ss');
    const currentFormatted = moment().format('YYYY-M-D HH:mm:ss');
    const oldDate = moment(oldFormatted);
    const currentDate = moment(currentFormatted);
    return oldDate.from(currentDate);
  }

}
